package ru.tsc.kyurinova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;
import ru.tsc.kyurinova.tm.enumerated.Role;

@Component
public class DataBase64SaveListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "data-save-base64";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save base64 data";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBase64SaveListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {        adminDataEndpoint.dataBase64Save(sessionService.getSession());
    }

    @Override
    public @Nullable
    Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
